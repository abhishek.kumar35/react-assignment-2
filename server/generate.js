module.exports = function(){
    var faker = require("faker");
    var _ = require("lodash");

    return{
        blogs:_.times(5, function(n){
            return{
                id:n+1,
                title:faker.lorem.sentence(12),
                description:faker.lorem.paragraphs(10),
                authorName:faker.name.findName()
            }
        })
    }
}