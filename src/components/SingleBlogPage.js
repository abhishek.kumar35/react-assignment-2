import React from "react";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

const SingleBlogPage = () => {
  const { userId } = useParams();
  const [singleBlogPost, setSingleBlogPost] = useState([]);

  const url = `http://localhost:3000/blogs/${userId}`;
  useEffect(() => {
    const fetchData = async () => {
      await fetch(url)
        .then((res) => res.json())
        .then((data) => {
          setSingleBlogPost(data);
        });
    };
    fetchData();
  }, []);

  return (
    <React.Fragment>
      <div className="container single-blog-post">
        <h1>{singleBlogPost.title}</h1>
        <p>by {singleBlogPost.authorName}</p>
        <h5>{singleBlogPost.description}</h5>
      </div>
    </React.Fragment>
  );
};

export default SingleBlogPage;
