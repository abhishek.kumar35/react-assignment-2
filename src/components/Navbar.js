import React from 'react'
import { NavLink } from 'react-router-dom'

const Navbar = () => {
  return (
    <React.Fragment>
        <nav>
            <NavLink to="/">Home</NavLink>
            <NavLink to="/about">About</NavLink>
            <NavLink to ="/contact">Contact</NavLink>

        </nav>

    </React.Fragment>
  )
}

export default Navbar